
[bar/bar1]

; Setup
width = 100%
height = 25
offset-y = 0
offset-x = 0
border-top-size = 0
border-bottom-size = 0
border-right-size = 0
border-left-size = 0
bottom = false
monitor = HDMI-0
override-redirect = false
font-0 = Hack:size=9.5;3
font-1 = "Font Awesome 5:style=Solid:pixelsize=12;3"
font-2 = "Font Awesome 5:style=Regular:pixelsize=12;3"
font-3 = "Font Awesome:pixelsize=12;3"
background = #cc2d2d2d
foreground = ${xrdb:color7}
border-color = 0

; stuff (bar1) ;
fixed-center = true
padding-left = 2
padding-right = 2
module-margin = 1.5
;modules-left = volume backlight battery wlan
modules-center = i3
modules-right =  spotify popupc
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

[bar/bar2]
; Setup
width = 100%
height = 25
offset-y = 0
offset-x = 0
border-top-size = 0
border-bottom-size = 0
border-right-size = 0
border-left-size = 0
bottom = false
monitor = DP-3
override-redirect = false
font-0 = Hack:size=9.5;3
font-1 = "Font Awesome 5:style=Solid:pixelsize=12;3"
font-2 = "Font Awesome 5:style=Regular:pixelsize=12;3"
font-3 = "Font Awesome:pixelsize=12;3"
background = #cc2d2d2d
foreground = ${xrdb:color7}
border-color = 0

; stuff Bar 2
fixed-center = true
padding-left = 2
padding-right = 2
module-margin = 1.5
modules-left = i3
modules-center = wired-network
modules-right = spotify memory popupc
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

[bar/bar3]

; Setup
width = 100%
height = 25
offset-y = 0
offset-x = 0
border-top-size = 0
border-bottom-size = 0
border-right-size = 0
border-left-size = 0
bottom = false
monitor = DP-1
override-redirect = false
font-0 = Hack:size=9.5;3
font-1 = "Font Awesome 5:style=Solid:pixelsize=12;3"
font-2 = "Font Awesome 5:style=Regular:pixelsize=12;3"
font-3 = "Font Awesome:pixelsize=12;3"
background = #cc2d2d2d
foreground = ${xrdb:color7}
border-color = 0

; stuff bar 3
fixed-center = true
padding-left = 2
padding-right = 2
module-margin = 1.5
modules-left = i3
modules-center = popupc
;modules-right = popupc
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev
tray-position = right
tray-detached = false
tray-padding = 1
tray-maxsize = 16

; MODULES GLOBAL

[module/i3]
type = internal/i3

pin-workspaces = true
strip-wsnumbers = true
enable-scroll = false

label-unfocused-foreground = ${xrdb:color7}
label-focused-foreground = ${xrdb:color6}
label-urgent-foreground = ${xrdb:color1}

[module/volume]
type = internal/alsa

format-volume = <ramp-volume> <label-volume>
format-muted =  0%

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

ramp-volume-foreground = ${xrdb:color6}
format-muted-foreground = ${xrdb:color1}

[module/memory]
type = internal/memory
format-prefix=" "
label = %gb_used%
format-prefix-foreground = ${xrdb:color2}
exec = raw=$(($(stat -f --format="%a*%S" .)));final=${raw:0:2};echo $final "GB"
tail = true

[module/popupc]
type = custom/script
format-prefix=" "
exec = ~/.config/polybar/popup-calendar.sh
interval = 5
click-left = ~/.config/polybar/popup-calendar.sh --popup
format-prefix-foreground = ${xrdb:color5}

[module/spotify]
type = custom/script
format-prefix=" "
interval = 5
exec = ~/.config/polybar/spotify.py
format-prefix-foreground = ${xrdb:color2}
label-foreground = ${xrdb:color2}

[module/wired-network]
type = internal/network
interface = enp3s0
format-connected = <label-connected>
format-disconnected = <label-disconnected>
format-packetloss = <animation-packetloss> <label-connected>
label-connected = %upspeed%|%downspeed%
label-connected-foreground = #eefafafa
label-disconnected = Net's Fucked!
label-disconnected-foreground = #66ffffff
label-packetloss = %linkspeed%
label-packetloss-foreground = #eefafafa
animation-packetloss-0 = ⚠
animation-packetloss-0-foreground = #ffa64c
animation-packetloss-1 = 📶
animation-packetloss-1-foreground = #000000
animation-packetloss-framerate = 500

[module/hackspeed]
type = custom/script
exec = ~/.config/polybar/info-hackspeed.sh
tail = true

;[module/vpn]
;type = custom/script
;exec = ~/.config/polybar/isrunning-openvpn.sh
;interval = 10

[module/i3]
;type = internal/i3
;format = <label-state> <label-mode>
;index-sort = true
wrapping-scroll = false
;label-focused = ●
;label-focused-padding = 2

;[module/spotify0]
;type = custom/script
;interval = 2
;format = <label>
;format-prefix = "  "
;click-left = xdotool key super+2
;exec = ~/.config/polybar/spot
;tail = true
;format-prefix-foreground = ${xrdb:color2}
;label-foreground = ${xrdb:color2}


